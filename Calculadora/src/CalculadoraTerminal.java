import java.util.Scanner;
/**
 * Clase para determinar las operaciones basicas de una
 * calculadora (suma, resta, multiplicacion, division)
 */
public class CalculadoraTerminal {
    public static void main(String[] args) {
        // Ingreso de datos por teclado
        Scanner consola = new Scanner(System.in);
        System.out.println("****** Calculadora version Consola 2.0 ******");

        // Bucle repetitivo while
        while(true) {
            // Menu principal
            System.out.println("""
                    1. Suma
                    2. Resta
                    3. Multiplicacion
                    4. Division
                    5. Salir
                    """);
            System.out.println("Por favor, ingrese opcion: ");
            try {
                var opcion = Integer.parseInt(consola.nextLine());
                System.out.println();
                // Validar opcion seleccionada
                if (opcion >= 1 && opcion <= 4) {
                    // Ingreso de numeros por consola para ejecutar operaciones
                    System.out.print("Ingrese valor del primer numero: ");
                    //var numero1 = Integer.parseInt(consola.nextLine());
                    var numero1 = Double.parseDouble(consola.nextLine());
                    System.out.print("Ingrese valor del segundo numero: ");
                    //var numero2 = Integer.parseInt(consola.nextLine());
                    var numero2 = Double.parseDouble(consola.nextLine());
                    System.out.println();

                    // Ejecutar operacion seleccionada
                    double resultado;
                    switch (opcion) {
                        case 1 -> { // Suma
                            resultado = numero1 + numero2;
                            System.out.println("El resultado de la suma es: " + resultado);
                        }
                        case 2 -> { // Resta
                            resultado = numero1 - numero2;
                            System.out.println("El resultado de la resta es: " + resultado);
                        }
                        case 3 -> { // Multiplicacion
                            resultado = numero1 * numero2;
                            System.out.println("El resultado de la multiplicacion es: " + resultado);
                        }
                        case 4 -> { // Division
                            if (numero2 != 0) {
                                resultado = numero1 / numero2;
                                System.out.println("El resultado de la division es: " + resultado);
                            } else {
                                System.out.println("No existe division para 0, intente nuevamente");
                            }
                        }
                        default -> {
                            System.out.println("Opcion ingresada no existe, por favor ingrese nuevamente.");
                        }
                    }

                } else if (opcion == 5) {
                    System.out.println("Gracias, hasta pronto!!!");
                    break;
                } else {
                    System.out.println("Opcion ingresada no existe, por favor ingrese nuevamente.");
                }
                System.out.println();
            } catch (NumberFormatException e) {
                //catch (Exception e){
                System.out.println();
                System.out.println("Opcion ingresada no es un valor permitido, por favor solamente ingrese numeros");
                System.out.println("Error en ejecucion de calculadora: " + e.getMessage());
                System.out.println();
            }
        }
    }
}


/*
* Ingreso por consola
* Clase Scanner
*
* Bucle repetitivo
* While
*
* Casting de valores para ejecutar operaciones
* ParseInt, ParseDouble
*
* Setencias de control
* If - Switch Case
*
* Manejo de excepciones para controlar posibles errores en ejecuci[on
* try catch
*
* */
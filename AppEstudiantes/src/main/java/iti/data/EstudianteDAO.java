package iti.data;

import iti.connect.Connect;
import iti.modelo.Estudiante;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static iti.connect.Connect.getConnect;

// DAO - Data Access Object
public class EstudianteDAO {

    public List<Estudiante> ListadoEstudiantes(){
        List<Estudiante> estudiantes = new ArrayList<>();

        // Trabajo con cclase de conexion a BD
        PreparedStatement ps;
        ResultSet rs;
        Connection conn = getConnect();
        String sql = "SELECT * FROM estudiante ORDER BY id_estudiante;";
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                var estudiante = new Estudiante();
                estudiante.setIdEstudiante(rs.getInt("id_estudiante"));
                estudiante.setCedula(rs.getString("cedula"));
                estudiante.setNombre(rs.getString("nombre"));
                estudiante.setApellido(rs.getString("apellido"));
                estudiante.setTelefono(rs.getString("telefono"));
                estudiante.setEmail(rs.getString("email"));
                estudiantes.add(estudiante);
            }
        }
        catch(SQLException e){
            System.out.println("Ocurrio un problema al listar estudiantes: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Errot al cerrar conexion: " + e.getMessage());
            }
        }

        return estudiantes;
    }

    public boolean findById(Estudiante estudiante){
        PreparedStatement ps;
        ResultSet rs;
        Connection conn = getConnect();
        String sql = "SELECT * FROM estudiante WHERE id_estudiante = ?;";
        try{
            ps = conn.prepareStatement(sql);
            ps.setInt(1, estudiante.getIdEstudiante());
            rs = ps.executeQuery();
            if(rs.next()){
                estudiante.setCedula(rs.getString("cedula"));
                estudiante.setNombre(rs.getString("nombre"));
                estudiante.setApellido(rs.getString("apellido"));
                estudiante.setTelefono(rs.getString("telefono"));
                estudiante.setEmail(rs.getString("email"));
                return true;
            }
        }
        catch(SQLException e){
            System.out.println("Errot al buscar estudiante: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Errot al cerrar conexion: " + e.getMessage());
            }
        }
        return false;
    }

    public boolean agragarEstudiante(Estudiante estudiante){
        PreparedStatement ps;
        Connection conn = getConnect();
        String sql = "INSERT INTO estudiante(cedula, nombre, apellido, telefono, email) " +
                "VALUES(?, ?, ?, ?, ?);";
        try{
            ps = conn.prepareStatement(sql);
            ps.setString(1, estudiante.getCedula());
            ps.setString(2, estudiante.getNombre());
            ps.setString(3, estudiante.getApellido());
            ps.setString(4, estudiante.getTelefono());
            ps.setString(5, estudiante.getEmail());
            ps.execute();
            return true;
        }
        catch(SQLException e){
            System.out.println("Errot al agregar estudiante: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Errot al cerrar conexion: " + e.getMessage());
            }
        }
        return false;
    }

    public boolean actualizaEstudiante(Estudiante estudiante){
        PreparedStatement ps;
        Connection conn = getConnect();
        String sql = "UPDATE estudiante SET cedula=?, nombre=?, apellido=?, telefono=?, email=? " +
                "WHERE id_estudiante=?;";
        try{
            ps = conn.prepareStatement(sql);
            ps.setString(1, estudiante.getCedula());
            ps.setString(2, estudiante.getNombre());
            ps.setString(3, estudiante.getApellido());
            ps.setString(4, estudiante.getTelefono());
            ps.setString(5, estudiante.getEmail());
            ps.setInt(6, estudiante.getIdEstudiante());
            ps.execute();
            return true;
        }
        catch(SQLException e){
            System.out.println("Errot al agregar estudiante: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Errot al cerrar conexion: " + e.getMessage());
            }
        }
        return false;
    }

    public boolean eliminaEstudiante(Estudiante estudiante){
        PreparedStatement ps;
        Connection conn = getConnect();
        String sql = "DELETE FROM estudiante WHERE id_estudiante=?;";
        try{
            ps = conn.prepareStatement(sql);
            ps.setInt(1, estudiante.getIdEstudiante());
            ps.execute();
            return true;
        }
        catch(SQLException e){
            System.out.println("Errot al eliminar estudiante: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Errot al cerrar conexion: " + e.getMessage());
            }
        }
        return false;
    }


    public static void main(String[] args){
        var estudianteDAO = new EstudianteDAO();


        // Buscar por ID
        /*var estudiante1 = new Estudiante(1);
        var existeEstudiante = estudianteDAO.findById(estudiante1);
        if(existeEstudiante)
            System.out.println("Estudiante encontrado: " + estudiante1);
        else
            System.out.println("No se encontro estudiante con ID: " + estudiante1.getIdEstudiante());*/

        // Agregar estudiante nuevo
        /*var nuevo_estudiante = new Estudiante("1716151413", "Mayra", "Salazar", "0909090909", "mayra@mail.com");
        var agrego = estudianteDAO.agragarEstudiante(nuevo_estudiante);
        if(agrego)
            System.out.println("Estudiante agregado exitosamente: " +  nuevo_estudiante);
        else
            System.out.println("Error al agregar estudiante: " + nuevo_estudiante);*/

        // MOdificar estudiante
        /*var edita_estudiante = new Estudiante(1, "1714151413", "Miguel", "Cervantes", "0998877561", "miguel@mail.com");
        var editado = estudianteDAO.actualizaEstudiante(edita_estudiante);
        if(editado)
            System.out.println("Estudiante actualizado exitosamente: " +  edita_estudiante);
        else
            System.out.println("Error al actualizar estudiante: " + edita_estudiante);*/

        // Eliminar estudiante
        var elimina_estudiante = new Estudiante(3);
        var eliminado = estudianteDAO.eliminaEstudiante(elimina_estudiante);
        if (eliminado)
            System.out.println("Estudiante eliminado exitosamente: " +  elimina_estudiante);
        else
            System.out.println("Error al eliminar estudiante: " + elimina_estudiante);

        // Listar estudiantes
        System.out.println("Lista de estudiantes");
        List<Estudiante> estudiantes = estudianteDAO.ListadoEstudiantes();
        estudiantes.forEach(System.out::println);
    }
}

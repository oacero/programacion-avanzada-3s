package iti.vista;

import iti.data.EstudianteDAO;
import iti.modelo.Estudiante;

import java.util.Scanner;

public class EstudiantesApp {
    public static void main(String[] args){
        Scanner consola = new Scanner(System.in);
        var salir = false;
        var estudianteDAO = new EstudianteDAO();

        while(!salir){
            try {
                muestraMenu();
                salir = ejecutaOpciones(consola, estudianteDAO);
            }
            catch(Exception e){
                System.out.println("Error al ejecutar opciones: " + e.getMessage());
            }
        }

    }

    public static void muestraMenu(){
        System.out.println();
        System.out.println("""
                .::** Sistema Estudiantes **::.
                1. Listar Estudiantes
                2. Buscar estudiante
                3. Agregar estudiante
                4. Editar estudiante
                5. Eliminar estudiante
                6. Salir
                """);
        System.out.println("Ingrese opcion");
    }

    public static boolean ejecutaOpciones(Scanner consola, EstudianteDAO estudianteDAO){
        var opcion = Integer.parseInt(consola.nextLine());
        System.out.println();
        boolean salida = false;

        switch (opcion){
            case 1 -> { // Listar estudiantes
                System.out.println();
                System.out.println("Listado de estudiantes");
                var estudiantesList = estudianteDAO.ListadoEstudiantes();
                estudiantesList.forEach(System.out::println);
            }
            case 2 -> { // Buscar por ID
                System.out.print("Ingrese ID de estudiante a buscar: ");
                var idEstudiante = Integer.parseInt(consola.nextLine());
                var estudiante = new Estudiante(idEstudiante);
                var existeEstudiante = estudianteDAO.findById(estudiante);
                if(existeEstudiante)
                    System.out.println("Estudiante encontrado: "+ estudiante);
                else
                    System.out.println("No se encontro estudiante con ID: " + estudiante);
            }
            case 3 -> { // Agregar estudiante
                System.out.println("Agregar Estudiante");
                System.out.print("Cedula: ");
                var cedula = consola.nextLine();
                System.out.print("Nombre: ");
                var nombre = consola.nextLine();
                System.out.print("Apellido: ");
                var apellido = consola.nextLine();
                System.out.print("Telefono: ");
                var telefono = consola.nextLine();
                System.out.print("Email: ");
                var email = consola.nextLine();

                var nuevoEstudiante = new Estudiante(cedula, nombre, apellido, telefono, email);
                var agrego = estudianteDAO.agragarEstudiante(nuevoEstudiante);
                if(agrego)
                    System.out.println("Estudiante agregado exitosamente: " +  nuevoEstudiante);
                else
                    System.out.println("Error al agregar estudiante: " + nuevoEstudiante);
            }
            case 4 -> { // Modificar estudiante
                System.out.println("Modificar Estudiante");
                System.out.print("Ingrese ID de estudiante a modificar: ");
                var idEstudiante = Integer.parseInt(consola.nextLine());
                System.out.print("Cedula: ");
                var cedula = consola.nextLine();
                System.out.print("Nombre: ");
                var nombre = consola.nextLine();
                System.out.print("Apellido: ");
                var apellido = consola.nextLine();
                System.out.print("Telefono: ");
                var telefono = consola.nextLine();
                System.out.print("Email: ");
                var email = consola.nextLine();

                var editaEstudiante = new Estudiante(idEstudiante, cedula, nombre, apellido, telefono, email);
                var editado = estudianteDAO.actualizaEstudiante(editaEstudiante);
                if(editado)
                    System.out.println("Estudiante actualizado exitosamente: " +  editaEstudiante);
                else
                    System.out.println("Error al actualizar estudiante: " + editaEstudiante);
            }
            case 5 -> { // Elimina estudiante
                System.out.println("Eliminar Estudiante");
                System.out.print("Ingrese ID de estudiante a modificar: ");
                var idEstudiante = Integer.parseInt(consola.nextLine());
                var eliminaEstudiante = new Estudiante(idEstudiante);
                var eliminado = estudianteDAO.eliminaEstudiante(eliminaEstudiante);
                if (eliminado)
                    System.out.println("Estudiante eliminado exitosamente: " +  eliminaEstudiante);
                else
                    System.out.println("Error al eliminar estudiante: " + eliminaEstudiante);
            }
            case 6 -> { // Salir
                System.out.println("Gracias, hasta pronto!!!");
                salida = true;
            }
            default -> System.out.println("Opcion ingresada no existe, por favor, vuelva a intentar");
        }
        return salida;
    }
}

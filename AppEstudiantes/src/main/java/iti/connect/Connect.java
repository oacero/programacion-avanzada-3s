package iti.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Clase de conexion  la BD
 *
 */
public class Connect {
    /**
     * Metodo para obtner objeto de conexion
     * @return Connection
     */
    public static Connection getConnect(){
        Connection connect = null;
        // definir datos para crear conexion
        var database = "app_estudiantes_db";
        var url = "jdbc:mysql://localhost:3306/" + database;
        var username = "root";
        var password = "";

        // Caragamos clase de conexion de driver mysql
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, username, password);
        }
        //catch(Exception e){
        catch(ClassNotFoundException | SQLException e){
            System.out.println("Error en conexion a BD: " + e.getMessage());
        }

        return connect;
    }

    public static void main(String[] args){
        var conexion =  Connect.getConnect();
        if(conexion != null )
            System.out.println("Conexion a BD de estudiantes satisfactoria!!! " + conexion);
        else
            System.out.println("Error en conexion a BD de estudiantes");
    }

}

# Programacion Avanzada 3s


## Comandos Git
### Clonar repositorio
```
git clone <url del repositorio>
git clone https://gitlab.com/oacero/programacion-avanzada-3s.git
```

### Estatus de cambios en archivos
```
git status
```

### Visualizar diferencias en archivos con respecto al archivo original
```
git diff <nombre del archivo a revisar>
```

## Commit (Subir) archivos con cambios al repositorio

### Agregar los archivos a confirmar en el repositorio
```
git add <archivos a confirmar> 
git add .  - preparar todos los archivos con cambios para ser confirmados
git add archivo.txt - preparar solo ese archivo para confirmar
```

### Confirmar cambios y agregar mensaje
```
git commit -m "mensaje"
```

### subir cambios a repositorio
```
git push -u origin main
```

### retornar al punto inicial entes de preparar para confirmar
```
git restore --staged <file>
```
### descartar cambios
```
git restore <file>
git checkout <file>
```

### descargar cambios desde repositorio remoto a repositorio local
```
git pull origin main
```
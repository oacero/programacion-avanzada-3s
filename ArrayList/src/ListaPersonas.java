import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListaPersonas {
    public static void main(String[] args){
        // Objeto para obtener datos ingresados por teclado
        Scanner consola = new Scanner(System.in);

        // Definir lista
        List<Persona> personas = new ArrayList<>();

        // Mostrar menu
        var finalizar = false;

        while (!finalizar){
            showMenu();
            try{
                finalizar = ejecutarOpciones(consola, personas);
            }
            catch(Exception e){
                System.out.println();
                System.out.println("Error en ejecucion: " + e.getMessage());
                System.out.println();
            }
            System.out.println();
        }
    }

    /**
     * Muestra menu de App
     */
    private static void showMenu(){
        System.out.println("""
                    ***** Lista de Personas *****
                    1. Agregar Persona
                    2. Mostrar listado de personas
                    3. Salir
                    """);
        System.out.println("Por favor, ingrese opcion: ");
    }

    /**
     * Ejecuta opciones de menu
     * @param consola
     * @param personas
     * @return boolean
     */
    private static boolean ejecutarOpciones(Scanner consola, List personas){
        var opcion = Integer.parseInt(consola.nextLine());
        System.out.println();
        boolean salida = false;

        // Validad opciones ingresadas
        switch (opcion){
            case 1 -> { // Agregar persona a listado
                System.out.print("Ingrese cedula: ");
                var cedula = consola.nextLine();
                System.out.print("Ingrese nombres: ");
                var nombres = consola.nextLine();
                System.out.print("Ingrese telefono: ");
                var telefono = consola.nextLine();
                System.out.print("Ingrese email: ");
                var email = consola.nextLine();

                // Creamos objeto Persona con datos ingresados
                var persona = new Persona(cedula, nombres, telefono, email);
                // Agregar al listado de personas
                personas.add(persona);
                System.out.println("La lista contiene actualmente " + personas.size() + " personas.");
                System.out.println();
            }
            case 2 -> { // Imprimir en patalla lista de personas ingresadas
                System.out.println(".:: Lista de personas ingresadas ::.");
                personas.forEach(System.out::println);
                System.out.println();
            }
            case 3 -> { // Terminar ejecucion
                System.out.println("Gracias, hasta pronto!!!");
                salida = true;
            }
            default ->  System.out.println("Opcion ingresada no existe, por favor ingrese nuevamente.");
        }
        return salida;
    }
}

package iti.libros_store.servicio;

import iti.libros_store.modelo.Libro;

import java.util.List;

public interface ILibroServicio {
    public List<Libro> listaLibros();
    public Libro buscarLibroPorId(Integer idLibro);
    public void guardarLibro(Libro libro);
    public void eliminarLibro(Libro libro);
}

public class Cuenta {
    // Atributos
    private double saldo;

    //Constructor
    public Cuenta(){
        this.saldo = 0.0;
    }

    public Cuenta(double saldo){
        this.saldo = saldo;
    }

    // Metodos
    public void setSaldo(double saldo){
        this.saldo = saldo;
    }

    public double getSaldo(){
        return this.saldo;
    }

    public void deposito(double monto){
        //this.saldo = this.saldo + monto;
        this.saldo += monto;
    }

    public void retiro(double monto){
        this.saldo -= monto;
    }
    
}

/*
 Una clase es la representacion de un objeto de la vida real, por medio de atributos y metodos
La clase se compone de 3 partes
- Nombre
- Atributos (propiedades y/o caracteristicas)
- Metodos o funciones (acciones que realiza el objeto)

El nombre de la clase de emepezar con Mayuscula
Ej:  
Vehiculos
AnimalesSalvajes

El archivo debe tener el mismo nombre de la clase y tener la extension .java
Ejemplo:
Vehiculos.java
AnimalesSalvajes.java

Definir una clase
Sintaxis

modificador_acceso class NombreClase{

}

public class Vehiculo{

}

Definicion de Atributos
Sintaxis
modificador_acceso tipo nombeAtributos = valor;
Ejm:
private double saldo;
public String nombre = 'Oscar';
protected int codigo = 10;
public boolean tieneDato = false;


Definicion de Metodos
modificador_acceso tipo_retorno nombreMetodo(tipo nombreParam1, tipo nombreParam2 ....){

}

Ejem:
public int sumar(int num1, int num2){
    return num1 + num2;
}

Constructores
Es un tipo de metodo especial que permite la construccion de objetos (instancias)
su finalidad es inicializar los atributos

Si no se define un constructor en la clase, java define un constructor por defecto que inciaializa los atributos 
con sus valores por defecto dependiendo del tipo de datos del atributo
Caracteristicas
- No tiene tipo de retorno
- Su nombre de ser igual al nombre de la clase

Sintaxis
modificador_acceso NombreClase(tipo param1, tipo param2...)[{
    // inicializa atributos
}

Ejemplo
public Cuenta(){
    this.saldo = 0.0;
}

Sobrecarga
Podemos que un metodo esta sobrecargado cuando existen2 o mas metodos con el mismo nombre y el mismo tipo de retorno
pero con un numero distinto de parametros
Ejemplo
Definicion de varios construcrtores para inciaializar el objeto

 */

/*
  Definicion de clase
 */
public class Principal{
    public static void main(String[] args){
        double saldo = 0;

        Cuenta cuenta1 = new Cuenta();
        Cuenta cuenta2 = new Cuenta(65.20);

        saldo = cuenta1.getSaldo();
        System.out.println("El saldo de la cuenta 1 es: " + saldo);

        saldo = cuenta2.getSaldo();
        System.out.println("El saldo en la cuenta 2 es: " + saldo);

        cuenta1.deposito(98.50);
        cuenta2.retiro(20);

        System.out.println("El nuevo saldo de la cuenta 1 es: " + cuenta1.getSaldo());
        System.out.println("El nuevo saldo de la cuenta 2 es: " + cuenta2.getSaldo());
    }
}


/*
 OBJETOS
 
 Los objetos son variables de un tipo complejo de datos (clase)

 Sintaxis
 tipo_dato identificador;
 Cuenta cuentaAhorros;

 Por defecto el objeto se inicializa con el valor null
 Para inicializar un objeto es distindo a inicializar una variable o atributo de tipo primitivo 
 Para incializar el objeto utlizamos el operador new
 Se llama al constructor de la clase que vamos a instanciar.

 Cuenta cuentaAhorros = new Cuenta();
 Cuenta cuentaAhorros2 = new Cuenta(50.0);
 
 
 */